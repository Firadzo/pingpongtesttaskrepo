﻿
using System;
using UnityEngine;

namespace PingPong.PlayerData
{
    [Serializable]
    public class PlayerProfile
    {
        private const string PlayerPrefsKey = "PlayerProfileSave";

        [SerializeField]
        private int m_BestScore;
        public int BestScore => m_BestScore;

        [SerializeField]
        private int m_BallColorIndex;
        public int BallColorIndex
        {
            get { return m_BallColorIndex; }
            set
            {
                if (m_BallColorIndex != value)
                {
                    m_BallColorIndex = value;
                    Save();
                }
            }
        }


        public static PlayerProfile Current { get; private set; }

        public static void Load()
        {
            if (Current == null)
            {
                var rawData = PlayerPrefs.GetString(PlayerPrefsKey, null);
                if (string.IsNullOrEmpty(rawData))
                {
                    Current = new PlayerProfile();
                }
                else
                {
                    try
                    {
                        Current = JsonUtility.FromJson<PlayerProfile>(rawData);
                        Debug.Log("PlayerProfile is loaded");
                    }
                    catch (Exception e)
                    {
                        Debug.LogErrorFormat("PlayerProfile load error: {0}", e.Message);
                    }
                    if (Current == null)
                    {
                        Current = new PlayerProfile();
                    }
                }
            }
        }

        public void Save()
        {
            PlayerPrefs.SetString(PlayerPrefsKey, JsonUtility.ToJson(Current));
        }

        public bool SaveScore(int score)
        {
            if (BestScore < score)
            {
                m_BestScore = score;
                Save();
                return true;
            }
            return false;
        }
    }
}
