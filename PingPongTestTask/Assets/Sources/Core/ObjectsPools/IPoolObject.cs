﻿
namespace PingPong.Core
{
    public interface IPoolObject
    {
        void Init();

        bool CanBeUsed
        {
            get;
        }
    }
}
