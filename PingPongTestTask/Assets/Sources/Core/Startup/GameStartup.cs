﻿
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PingPong.Core
{
    public class GameStartup : MonoBehaviour
    {
        private static bool GameStarted;

        public static void StartTheGame()
        {
            if (GameStarted)
            {
                return;
            }
            GameObject gameObject = new GameObject("--GAME_STARTER--");
            GameObject.DontDestroyOnLoad(gameObject);
            gameObject.AddComponent<GameStartup>().LoadGameScene();
            GameStarted = true;
        }

        private void LoadGameScene()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
            SceneManager.LoadScene("GameScene");
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
            The.ScreenFadeEffect.PlayFadeOut(OnFadeOut);
        }

        private void OnFadeOut()
        {
            Destroy(gameObject);
        }
    }
}
