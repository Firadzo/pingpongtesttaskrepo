﻿
using UnityEngine;
using PingPong.Game.InputSytem;
using PingPong.PlayerData;

namespace PingPong.Core.Startup
{
    public class GameInit : MonoBehaviour
    {
        private void Awake()
        {
            PlayerProfile.Load();
            var updatesSystem = UpdatesSystem.CreateInstanceIfNone(false);
            updatesSystem.AddSystem(InputController.Create());
            updatesSystem.AddSystem(CommonUpdatableHolder.Create());

            GameStartup.StartTheGame();
        }
    }
}
