﻿
using UnityEngine;
using PingPong.Core;
using PingPong.PlayerData;
using PingPong.Game;
using PingPong.Game.Effects;
using PingPong.Game.InputSytem;
using PingPong.Game.UI;

namespace PingPong
{
    public static class The
    {
        public static CommonUpdatableHolder CommonUpdatableHolder => CommonUpdatableHolder.Current;
        public static PlayerProfile PlayerProfile => PlayerProfile.Current;
        public static InputController InputController => InputController.Instance;
        public static EventManager EventManager => EventManager.Current;

        public static UIManager UIManager => UIManager.Instance;
        public static GameField GameField => GameField.Current;

        public static ScreenFadeEffect ScreenFadeEffect => ScreenFadeEffect.Instance;
    }
}
