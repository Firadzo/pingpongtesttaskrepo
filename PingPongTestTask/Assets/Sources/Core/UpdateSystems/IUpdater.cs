﻿
namespace PingPong.Core
{
    public interface IUpdater
    {
        void AddToUpdate(IUpdatable @object);
        void RemoveFromUpdate(IUpdatable @object);
    }

    public static class IUpdaterExtensions
    {
        //public static BaseTimer CallActionWithTimer(this IUpdater updater, float delay, System.Action action)
        //{
        //    var timer = new BaseTimer(delay, true, action);
        //    updater.AddToUpdate(timer);
        //    return timer;
        //}
    }
}
