﻿
using System.Collections.Generic;

namespace PingPong.Core
{
    public abstract class BaseUpdateSystem : IUpdatable, IUpdater
    {
        protected List<IUpdatable> updatables;

        protected abstract bool PauseUpdate { get; }

        public bool CanBeRemovedFromUpdate
        {
            get { return false; }
        }

        public void AddToUpdate(IUpdatable @object)
        {
            if (!updatables.Contains(@object))
            {
                updatables.Add(@object);
            }
        }

        public void RemoveFromUpdate(IUpdatable @object)
        {
            updatables.Remove(@object);
        }

        public void Clear()
        {
            updatables.Clear();
        }

        public void UpdateObject(float deltaTime)
        {
            if (PauseUpdate)
            {
                return;
            }
            for (int i = 0; i < updatables.Count; i++)
            {
                if (updatables[i] == null || updatables[i].CanBeRemovedFromUpdate)
                {
                    updatables.RemoveAt(i);
                    i--;
                    continue;
                }
                updatables[i].UpdateObject(deltaTime);
            }
        }
    }
}
