﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PingPong.Core
{
    public class UpdatesSystem : MonoSingleton<UpdatesSystem>
    {
        private List<IUpdatable> systems = new List<IUpdatable>();
        private int systemsCount;

        public void AddSystem(IUpdatable updatableSystem)
        {
            systems.Add(updatableSystem);
            systemsCount++;
        }

        private void Update()
        {
            var timeDelta = Time.deltaTime;
            for (int i = 0; i < systemsCount; i++)
            {
                systems[i].UpdateObject(timeDelta);
            }
        }
    }
}
