﻿
namespace PingPong.Core
{
    public interface IUpdatable
    {
        bool CanBeRemovedFromUpdate
        {
            get;
        }

        void UpdateObject(float deltaTime);
    }
}
