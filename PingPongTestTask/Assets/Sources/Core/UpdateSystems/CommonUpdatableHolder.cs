﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PingPong.Core
{
    public class CommonUpdatableHolder : BaseUpdateSystem
    {
        public static CommonUpdatableHolder Current { get; private set; }

        protected override bool PauseUpdate => false;

        public static CommonUpdatableHolder Create()
        {
            if (Current == null)
            {
                Current = new CommonUpdatableHolder();
                Current.updatables = new List<IUpdatable>();
            }
            return Current;
        }
    }
}
