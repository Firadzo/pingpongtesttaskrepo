﻿
using UnityEngine;

namespace PingPong.Core
{
    public abstract class BaseUpdatable : MonoBehaviour, IUpdatable
    {
        private bool subcribedToUpdate;

        virtual public bool CanBeRemovedFromUpdate
        {
            get { return !gameObject.activeSelf; }
        }

        abstract public BaseUpdateSystem UpdateSystem { get; }

        virtual protected void OnEnable()
        {
            AddToUpdate();
        }

        virtual protected void OnDisable()
        {
            RemovedFromUpdate();
        }

        protected void AddToUpdate()
        {
            if (!subcribedToUpdate)
            {
                subcribedToUpdate = true;
                UpdateSystem.AddToUpdate(this);
            }
        }

        protected void RemovedFromUpdate()
        {
            if (subcribedToUpdate)
            {
                subcribedToUpdate = false;
                UpdateSystem.RemoveFromUpdate(this);
            }
        }

        abstract public void UpdateObject(float deltaTime);
    }
}
