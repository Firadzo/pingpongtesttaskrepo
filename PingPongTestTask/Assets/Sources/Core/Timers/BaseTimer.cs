﻿
using UnityEngine;

namespace PingPong.Core
{
    [System.Serializable]
    public class BaseTimer : IPoolObject, IUpdatable
    {
        protected float timer;
        [SerializeField]
        protected float time;
        protected System.Action callBack;
        protected bool active;
        protected bool useUnscaledTime;

        public float GetTime => time;
        public virtual float NormalizedProgress => timer / time;
        public virtual float TimeLeft => time - timer;
        public bool isActive => active;

        //IUpdatable
        public bool CanBeRemovedFromUpdate => !active;

        // IPoolObject implementation
        public bool CanBeUsed => !active;

        public void Init()
        {
        }

        public BaseTimer()
        {
        }

        public BaseTimer(float time, bool start = false, System.Action callBack = null, bool useUnscaledTime = false)
        {
            timer = 0;
            this.time = time;
            this.callBack = callBack;
            active = start;
            this.useUnscaledTime = useUnscaledTime;
        }

        public void AddCallback(System.Action _callBack)
        {
            callBack += _callBack;
        }

        public virtual void Start()
        {
            Start(time);
        }

        public virtual void Start(float t)
        {
            active = true;
            timer = 0;
            time = t;
        }

        public void Start(float t, System.Action _callBack)
        {
            callBack = _callBack;
            Start(t);
        }

        public void Stop()
        {
            active = false;
        }

        public void Resume()
        {
            active = true;
        }

        public virtual bool Update()
        {
            return Update(useUnscaledTime ? Time.unscaledDeltaTime : Time.deltaTime);
        }

        public virtual bool Update(float timeDelta)
        {
            if (active)
            {
                timer += timeDelta;
                if (timer >= time)
                {
                    active = false;
                    if (callBack != null)
                    {
                        callBack();
                    }
                    return !active;
                }
            }
            return false;
        }

        /// <summary>
        /// For IUpdatable
        /// </summary>
        public void UpdateObject(float timeDelta)
        {
            Update(timeDelta);
        }
    }
}
