﻿
public class ObjectWithStaticReference<T> where T : new()
{
    protected static T m_current;
    public static T Current
    {
        get
        {
            return CreateInstanceIfNone();
        }
    }

    public static T CreateInstanceIfNone()
    {
        if (m_current == null)
        {
            m_current = new T();
        }
        return m_current;
    }
}