﻿
using UnityEngine;

public class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
    protected static T s_Instance;
    public static T Instance
    {
        get
        {
            CreateInstanceIfNone();
            return s_Instance;
        }
    }

    protected bool IsOtherInstanceExists
    {
        get
        {
            return (s_Instance != null && s_Instance.GetInstanceID() != this.GetInstanceID());
        }
    }

    public static T CreateInstanceIfNone(bool searchForInstanceOnSceneBeforeCreating = true)
    {
        if (s_Instance == null)
        {
            if (searchForInstanceOnSceneBeforeCreating)
            {
                s_Instance = GameObject.FindObjectOfType(typeof(T)) as T;
                if (s_Instance == null)
                {
                    CreateNewInstance();
                }
            }
            else
            {
                CreateNewInstance();
            }
        }
        return s_Instance;
    }

    protected static void CreateNewInstance()
    {
        GameObject gameObject = new GameObject(typeof(T).Name);
        GameObject.DontDestroyOnLoad(gameObject);
        s_Instance = gameObject.AddComponent(typeof(T)) as T;
    }

    virtual protected void Awake()
    {
        if (IsOtherInstanceExists)
        {
            Destroy(this.gameObject);
            return;
        }
        s_Instance = this as T;
    }
}