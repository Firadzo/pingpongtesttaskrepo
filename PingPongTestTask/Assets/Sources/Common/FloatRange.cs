﻿using UnityEngine;

[System.Serializable]
public class FloatRange
{
    public float min;
    public float max;

    public FloatRange()
    {
    }

    public FloatRange(float min, float max)
    {
        this.min = min;
        this.max = max;
    }

    public float Lerp(float t)
    {
        return Mathf.Lerp(min, max, t);
    }

    public float LerpUnclamped(float t)
    {
        return Mathf.LerpUnclamped(min, max, t);
    }

    public float GetInverseLerpValue(float value)
    {
        return Mathf.InverseLerp(min, max, value);
    }

    public float Random
    {
        get
        {
            return UnityEngine.Random.Range(min, max);
        }
    }
}

