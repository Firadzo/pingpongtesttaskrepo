﻿
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using PingPong.Core;

namespace PingPong.Game
{
    public class NetworkMatchSearcher : MonoBehaviourPunCallbacks, IUpdatable
    {
        private enum EState
        {
            None, ConnectingToMaster, SearchingForMatch, CreatingMatch, MatchCreated, JoinedToMatch, Stopping
        }

        public event System.Action OnMatchWasFound;
        public event System.Action OnPlayerWasDisconnected;

        private bool connectedToMaster;
        private EState currentState;

        public bool CanBeRemovedFromUpdate { get; private set; }

        public static NetworkMatchSearcher Current { get; private set; }

        public static NetworkMatchSearcher Run()
        {
            if (Current == null)
            {
                Current = new GameObject("<<--NetworkMatchSearcher-->").AddComponent<NetworkMatchSearcher>();
            }
            return Current;
        }

        private void Awake()
        {
            if (Current != null && Current.GetInstanceID() != this.GetInstanceID())
            {
                Destroy(gameObject);
                return;
            }
            Current = this;
            if (!PhotonNetwork.IsConnected)
            {
                PhotonNetwork.GameVersion = "1.0";
                PhotonNetwork.ConnectUsingSettings();
                currentState = EState.ConnectingToMaster;
            }
            else
            {
                Debug.Log("NetworkMatchSearcher: <b>Already connected to master</b>");
                connectedToMaster = true;
                SearchForMatch();
            }
            The.CommonUpdatableHolder.AddToUpdate(this);
        }

        public override void OnConnectedToMaster()
        {
            Debug.Log("NetworkMatchSearcher: <b>OnConnectedToMaster</b>");
            connectedToMaster = true;
            currentState = EState.None;
            base.OnConnectedToMaster();
            SearchForMatch();
        }

        public void Stop()
        {
            Debug.LogFormat("NetworkMatchSearcher: <b>Stop</b> current state: {0}", currentState);
            if (currentState == EState.Stopping)
            {
                return;
            }
            currentState = EState.Stopping;
            Current = null;
            The.CommonUpdatableHolder.RemoveFromUpdate(this);
            PhotonNetwork.LeaveRoom();
            GameObject.Destroy(gameObject);
        }

        private void SearchForMatch()
        {
            currentState = EState.SearchingForMatch;
            PhotonNetwork.JoinRandomRoom();
        }

        public void UpdateObject(float deltaTime)
        {
            if (connectedToMaster)
            {
                if (currentState == EState.None)
                {
                    Debug.Log("Createing new match...");
                    currentState = EState.CreatingMatch;
                    PhotonNetwork.CreateRoom(null, new RoomOptions() { MaxPlayers = 2 });
                }
            }
            if (PhotonNetwork.InRoom && PhotonNetwork.CurrentRoom.PlayerCount == 2)
            {
                OnMatchWasFound.InvokeSafely();
                The.CommonUpdatableHolder.RemoveFromUpdate(this);
            }
        }

        public override void OnCreatedRoom()
        {
            Debug.LogFormat("NetworkMatchSearcher: <b>OnCreatedRoom</b>");
            currentState = EState.MatchCreated;
            base.OnCreatedRoom();
        }

        public override void OnCreateRoomFailed(short returnCode, string message)
        {
            Debug.LogErrorFormat("NetworkMatchSearcher: <b>OnCreateRoomFailed</b>\n{0}", message);
            base.OnCreateRoomFailed(returnCode, message);
            currentState = EState.None;
        }

        public override void OnJoinedRoom()
        {
            currentState = EState.JoinedToMatch;
            Debug.Log("NetworkMatchSearcher: <b>OnJoinedRoom</b>");
            base.OnJoinedRoom();
        }

        public override void OnJoinRoomFailed(short returnCode, string message)
        {
            currentState = EState.None;
            Debug.LogErrorFormat("NetworkMatchSearcher: <b>OnJoinRoomFailed</b>\n{0}", message);
            base.OnJoinRoomFailed(returnCode, message);
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            currentState = EState.None;
            Debug.LogErrorFormat("NetworkMatchSearcher: <b>OnJoinRandomFailed</b>\n{0}", message);
            base.OnJoinRandomFailed(returnCode, message);
        }

        public override void OnLeftRoom()
        {
            Debug.Log("NetworkMatchSearcher: <b>OnLeftRoom</b>");
            base.OnLeftRoom();
            OnPlayerWasDisconnected.InvokeSafely();
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            Debug.LogFormat("NetworkMatchSearcher: <b>OnDisconnected</b>\n{0}", cause);
            base.OnDisconnected(cause);
            OnPlayerWasDisconnected.InvokeSafely();
        }

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            Debug.Log("NetworkMatchSearcher: <b>OnPlayerLeftRoom</b>");
            base.OnPlayerLeftRoom(otherPlayer);
            OnPlayerWasDisconnected.InvokeSafely();
        }
    }
}
