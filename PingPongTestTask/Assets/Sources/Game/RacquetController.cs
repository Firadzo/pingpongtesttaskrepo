﻿
using UnityEngine;

namespace PingPong.Game
{
    public class RacquetController : MonoBehaviour
    {
        private float moveSpeed;
        private float halfRacquetWidth;
        private Vector3 currentPosition;

        public float PositionByX => currentPosition.x;

        public void Setup(Vector3 initialPosition, float moveSpeed)
        {
            gameObject.SetActive(true);
            this.moveSpeed = moveSpeed;

            var size = GetComponent<Renderer>().bounds.size;
            halfRacquetWidth = size.x * 0.5f;
            currentPosition = initialPosition;
            var top = initialPosition.y > 0f;
            currentPosition.y += top ? -(size.y * 0.5f) : (size.y * 0.5f);
            transform.position = currentPosition;
        }

        public void SetupByY(float y)
        {
            var size = GetComponent<Renderer>().bounds.size;
            y += y > 0 ? -(size.y * 0.5f) : (size.y * 0.5f);
            currentPosition.y = y;
            transform.position = currentPosition;
        }

        public void SetupByX(float x)
        {
            currentPosition = transform.position;
            currentPosition.x = x;
            transform.position = currentPosition;
        }

        public void UpdateObject(float deltaTime, float moveDirection)
        {
            if (moveDirection != 0)
            {
                currentPosition.x += moveDirection * moveSpeed * deltaTime;
                if (currentPosition.x - halfRacquetWidth < The.GameField.LeftBorder)
                {
                    currentPosition.x = The.GameField.LeftBorder + halfRacquetWidth;
                }
                else if (currentPosition.x + halfRacquetWidth > The.GameField.RightBorder)
                {
                    currentPosition.x = The.GameField.RightBorder - halfRacquetWidth;
                }

                transform.position = currentPosition;
            }
        }
    }
}
