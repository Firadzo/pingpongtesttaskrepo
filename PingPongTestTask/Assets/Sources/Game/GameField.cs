﻿
using UnityEngine;

namespace PingPong.Game
{
    public class GameField : ObjectWithStaticReference<GameField>
    {
        private const float ReferenceWidth = 1920f;
        private const float ReferenceHeight = 1080;

        private const float BorderXSize = 200f;

        private const string BallPrefabPath = "GameField/Ball";
        private const string RacquetePrefabPath = "GameField/Racquet";

        private BoxCollider2D leftBorderCollider;
        private BoxCollider2D rightBorderCollider;

        private float currentAspect;

        public float Aspect => Width / Height;
        public float Width => ReferenceWidth;
        public float Height { get; private set; }

        public float TopBorder { get; private set; }
        public float BotBorder { get; private set; }
        public float LeftBorder { get; private set; }
        public float RightBorder { get; private set; }

        public BallController Ball { get; private set; }
        public RacquetController BotRacquet { get; private set; }
        public RacquetController TopRacquet { get; private set; }

        private Transform m_gameFieldParent;
        private Transform GameFieldParent
        {
            get
            {
                if (m_gameFieldParent == null)
                {
                    m_gameFieldParent = new GameObject("GameField").transform;
                }
                return m_gameFieldParent;
            }
        }

        public GameField()
        {
            RecalculateBorders();
            leftBorderCollider = SetupFieldBorder(LeftBorder, Height);
            rightBorderCollider = SetupFieldBorder(RightBorder, Height);
        }

        private void RecalculateBorders()
        {
            var camera = Helpers.MainCamera;
            var pixelRect = camera.pixelRect;

            var heightToWidthRatio = pixelRect.height / pixelRect.width;

            //Keeping constant field width scaling only by y-axis
            Height = ReferenceWidth * heightToWidthRatio;

            var halfWidth = ReferenceWidth * 0.5f;
            var halfHeight = Height * 0.5f;
            camera.orthographicSize = halfHeight;

            Debug.Log(pixelRect + "  " + heightToWidthRatio + " " + halfHeight);

            TopBorder = halfHeight;
            BotBorder = -halfHeight;
            LeftBorder = -halfWidth;
            RightBorder = halfWidth;
        }

        public void SetupGameField()
        {
            RecalculateBorders();
            Ball = CreateObjectIfNone(BallPrefabPath, Ball);
            BotRacquet = CreateObjectIfNone(RacquetePrefabPath, BotRacquet);
            TopRacquet = CreateObjectIfNone(RacquetePrefabPath, TopRacquet);
        }

        public void ScaleFieldByAspect(float aspect)
        {
            var camera = Helpers.MainCamera;
            var pixelRect = camera.pixelRect;

            Height = ReferenceWidth / aspect;
            var halfHeight = Height * 0.5f;

            if (camera.aspect > aspect)
            {
                camera.orthographicSize = halfHeight;

                var ratio = aspect / camera.aspect;
                var borderWidth = (1f - ratio) * ReferenceWidth * 0.5f;
                The.UIManager.ShowBorders(borderWidth);
            }

            TopBorder = halfHeight;
            BotBorder = -halfHeight;
            if (BotRacquet != null)
            {
                BotRacquet.SetupByY(BotBorder);
            }
            if (TopRacquet != null)
            {
                TopRacquet.SetupByY(TopBorder);
            }
        }

        public void Clear()
        {
            GameObject.Destroy(Ball.gameObject);
            GameObject.Destroy(BotRacquet.gameObject);
            GameObject.Destroy(TopRacquet.gameObject);
        }

        private BoxCollider2D SetupFieldBorder(float xPosition, float height)
        {
            var isLeftBorder = xPosition < 0f;
            var gameObject = new GameObject(isLeftBorder ? "LeftBorder" : "RightBorder");
            var collider = gameObject.AddComponent<BoxCollider2D>();

            RefreshFieldBorder(collider, xPosition, height);
            return collider;
        }

        private void RefreshFieldBorder(BoxCollider2D borderCollider, float xPosition, float height)
        {
            var isLeftBorder = xPosition < 0f;
            var position = borderCollider.transform.position;
            var offset = isLeftBorder ? -BorderXSize * 0.5f : BorderXSize * 0.5f;
            position.x = xPosition + offset;
            borderCollider.transform.position = position;

            var size = new Vector2(BorderXSize, height);
            borderCollider.size = size;
        }

        private T CreateObjectIfNone<T>(string resourceFilePath, T current) where T : Component
        {
            if (current == null)
            {
                current = GameObject.Instantiate(Resources.Load(resourceFilePath) as GameObject, GameFieldParent).GetComponent<T>();
                current.gameObject.SetActive(false);
            }
            return current;
        }
    }
}
