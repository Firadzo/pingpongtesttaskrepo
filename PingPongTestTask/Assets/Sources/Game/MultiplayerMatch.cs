﻿
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using PingPong.Core;

namespace PingPong.Game
{
    public class MultiplayerMatch : SinglePlayerMatch, IUpdatable, IOnEventCallback
    {
        private const int MaxScoreToWin = 4;

        private const byte PauseEventCode = 1;
        private const byte ResumeEventCode = 2;
        private const byte YouWinCode = 3;
        private const byte YouLoseCode = 4;


        private bool isGameFieldSizeSynced;

        override public int Score
        {
            get
            {
                if (MatchDataNetworkSync.Current == null)
                { return 0; }
                return IsHost ? MatchDataNetworkSync.Current.player1Score : MatchDataNetworkSync.Current.player2Score;
            }
        }

        public int OpponentScore
        {
            get
            {
                if (MatchDataNetworkSync.Current == null)
                { return 0; }
                return IsHost ? MatchDataNetworkSync.Current.player2Score : MatchDataNetworkSync.Current.player1Score;
            }
        }

        private bool IsHost => PhotonNetwork.IsMasterClient;

        private float ThisInput => PhotonNetwork.IsMasterClient ? PlayerInputSync.This.playerInput : PlayerInputSync.Other.playerInput;
        private float OtherInput => PhotonNetwork.IsMasterClient ? PlayerInputSync.Other.playerInput : PlayerInputSync.This.playerInput;

        override public void PrepareGameField()
        {
            Debug.LogFormat("MultiplayerMatch. IsHost: {0}", IsHost);
            base.PrepareGameField();
            topRacquet.GetComponent<SpriteRenderer>().color = Constants.OpponentColor;
            PhotonNetwork.Instantiate("Network/PlayerInputSync", Vector3.zero, Quaternion.identity);
            PhotonNetwork.Instantiate("Network/RacquetNetworkPositionSync", Vector3.zero, Quaternion.identity);
            if (IsHost)
            {
                PhotonNetwork.Instantiate("Network/MatchDataNetworkSync", Vector3.zero, Quaternion.identity);
                PhotonNetwork.Instantiate("Network/BallNetworkPositionSync", Vector3.zero, Quaternion.identity);
                var hostDataNetworkSync = PhotonNetwork.Instantiate("Network/HostDataNetworkSync", Vector3.zero, Quaternion.identity).GetComponent<HostDataNetworkSync>();
                hostDataNetworkSync.fieldAspect = GameField.Current.Aspect;
                hostDataNetworkSync.SendData();
            }
            else
            {
                Component.Destroy(ball.GetComponent<Collider2D>());
               // Component.Destroy(ball.GetComponent<Rigidbody2D>());
                ball.enabled = false;
                SyncGameFieldSize();
            }
        }

        override public void Restart()
        {
            if (IsHost)
            {
                SetupNewMatch();
                ballActivationDelayTimer.Start(BallPushDelay);
                ball.Push();
            }
        }

        override public void Start()
        {
            if (isStarted)
            {
                Debug.LogError("Match already started");
                return;
            }
            score = 0;
            IsPaused = false;
            isStarted = true;

            if (IsHost)
            {
                ballActivationDelayTimer.Start(BallPushDelay);
                ball.HitedByPlayerRacquete += OnBallHitedByPlayerRacquete;
                ball.MovedOutOfGameField += OnBallMovedOutOfGameField;
                ball.Push();
            }
            NetworkMatchSearcher.Current.OnPlayerWasDisconnected += OnPlayerDisconnected;
            PhotonNetwork.AddCallbackTarget(this);
            The.CommonUpdatableHolder.AddToUpdate(this);
            The.InputController.OnBackKeyPressed += OnBackKeyPressed;
        }

        override public void Stop()
        {
            if (isStarted)
            {
                isStarted = false;

                if (IsHost)
                {
                    ball.HitedByPlayerRacquete -= OnBallHitedByPlayerRacquete;
                    ball.MovedOutOfGameField -= OnBallMovedOutOfGameField;
                    The.InputController.OnBackKeyPressed -= OnBackKeyPressed;
                }
                NetworkMatchSearcher.Current.OnPlayerWasDisconnected -= OnPlayerDisconnected;
                PhotonNetwork.RemoveCallbackTarget(this);
                if (NetworkMatchSearcher.Current != null)
                {
                    NetworkMatchSearcher.Current.Stop();
                }
                The.CommonUpdatableHolder.RemoveFromUpdate(this);
            }
        }

        public override void TogglePauseState(bool pauseEnabled)
        {
            base.TogglePauseState(pauseEnabled);
            PlayerInputSync.This.isPause = IsPaused;
            PlayerInputSync.This.SendData();
            PhotonNetwork.RaiseEvent(IsPaused ? PauseEventCode : ResumeEventCode, null, RaiseEventOptions.Default, SendOptions.SendReliable);
        }

        private void OnPlayerDisconnected()
        {
            if (isStarted)
            {
                TogglePauseState(true);
                The.UIManager.ShowPlayerDisconnectedUI();
            }
        }

        private void OnBackKeyPressed()
        {
            if (isStarted && !IsPaused)
            {
                TogglePauseState(true);
                The.UIManager.ShowPauseUI(true);
            }
        }

        public void OnEvent(EventData photonEvent)
        {
            if (isStarted)
            {
                if (photonEvent.Code == PauseEventCode)
                {
                    OnBackKeyPressed();
                }
                else if (photonEvent.Code == ResumeEventCode)
                {
                    var currentMenu = The.UIManager.CurrentMenu;
                    if (currentMenu is UI.PauseMenu)
                    {
                        currentMenu.OnBackKeyPressed();
                    }
                }
                else if (!IsHost)
                {
                    if (photonEvent.Code == YouWinCode || photonEvent.Code == YouLoseCode)
                    {
                        Stop();
                        The.UIManager.ShowMultiplayerEndGameMenu(photonEvent.Code == YouWinCode);
                    }
                }
            }
        }

        private void OnBallHitedByPlayerRacquete()
        {
            if (currentBallSpeedFactor < gameParameters.MaxBallSpeedFactor)
            {
                currentBallSpeedFactor += gameParameters.BallSpeedFactorChangeStep;
            }
            else
            {
                currentBallSpeedFactor = gameParameters.MaxBallSpeedFactor;
            }
        }

        private void OnBallMovedOutOfGameField(bool isTop)
        {
            if (IsHost)
            {
                if (isTop)
                {
                    MatchDataNetworkSync.Current.player1Score++;
                    Debug.Log("player1Score " + MatchDataNetworkSync.Current.player1Score);
                }
                else
                {
                    MatchDataNetworkSync.Current.player2Score++;
                    Debug.Log("player2Score " + MatchDataNetworkSync.Current.player2Score);
                }
                MatchDataNetworkSync.Current.SendData();

                if (MatchDataNetworkSync.Current.player1Score == MaxScoreToWin || MatchDataNetworkSync.Current.player2Score == MaxScoreToWin)
                {
                    var player1Win = MatchDataNetworkSync.Current.player1Score == MaxScoreToWin;
                    The.UIManager.ShowMultiplayerEndGameMenu(player1Win);
                    NetworkMatchSearcher.Current.OnPlayerWasDisconnected -= OnPlayerDisconnected;
                    PhotonNetwork.RemoveCallbackTarget(this);
                    The.CommonUpdatableHolder.RemoveFromUpdate(this);
                    PhotonNetwork.RaiseEvent(player1Win ? YouLoseCode : YouWinCode, null, RaiseEventOptions.Default, SendOptions.SendReliable);
                }
                else
                {
                    Restart();
                }
            }
        }

        private void SyncGameFieldSize()
        {
            if (!isGameFieldSizeSynced && HostDataNetworkSync.Current != null)
            {
                var fieldAspect = HostDataNetworkSync.Current.fieldAspect;
                if (fieldAspect > 0)
                {
                    isGameFieldSizeSynced = true;
                    if (The.GameField.Aspect != fieldAspect)
                    {
                        The.GameField.ScaleFieldByAspect(fieldAspect);
                    }
                }
            }
        }

        override public void UpdateObject(float deltaTime)
        {
            if (IsPaused || PlayerInputSync.Other.isPause || !PhotonNetwork.IsConnected)
            {
                return;
            }

            var moveDirection = 0f;

            if (Input.GetMouseButton(0))
            {
                var tapWorldPosition = Helpers.MainCamera.ScreenToWorldPoint(Input.mousePosition);
                moveDirection = tapWorldPosition.x > 0f ? 1f : -1f;
            }
#if UNITY_EDITOR || UNITY_STANDALONE
            else
            {
                var xAxis = Input.GetAxis("Horizontal");
                if (Mathf.Abs(xAxis) > 0.1f)
                {
                    moveDirection = xAxis > 0f ? 1f : -1f;
                }
            }
#endif
            PlayerInputSync.This.playerInput = moveDirection;
            PlayerInputSync.This.SendData();


            botRacquet.UpdateObject(deltaTime, moveDirection);
            topRacquet.SetupByX(-RacquetNetworkPositionSync.Other.X); //For connected player field is upside down
            RacquetNetworkPositionSync.This.X = botRacquet.PositionByX;

            if (IsHost)
            {
                if (ballActivationDelayTimer.isActive)
                {
                    ballActivationDelayTimer.Update(deltaTime);
                }
                else
                {
                    ball.UpdateObject(deltaTime, currentBallSpeed * currentBallSpeedFactor);
                }
                BallNetworkPositionSync.Current.Sync();
            }
            else
            {
                SyncGameFieldSize();
            }
        }
    }
}
