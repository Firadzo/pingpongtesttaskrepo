﻿
using UnityEngine;

namespace PingPong.Game.Effects
{
    public class ScreenFadeEffect : MonoBehaviour
    {
        public event System.Action OnFadedIn;

        private const float DefaultAnimationTime = 0.4f;
        private const string ObjectInResourcesPath ="UI/ScreenFadeEffect";

        [SerializeField]
        private AlphaColorAnimation alphaColorAnimation;

        public bool IsPlaying => alphaColorAnimation.IsPlaying;

        protected static ScreenFadeEffect s_Instance;
        public static ScreenFadeEffect Instance
        {
            get
            {
                CreateInstanceIfNone();
                return s_Instance;
            }
        }

        private static void CreateInstanceIfNone()
        {
            if (s_Instance == null)
            {
                s_Instance = Instantiate((Resources.Load(ObjectInResourcesPath) as GameObject)).GetComponent<ScreenFadeEffect>();
                GameObject.DontDestroyOnLoad(s_Instance.gameObject);
                s_Instance.alphaColorAnimation.Init();
                s_Instance.gameObject.SetActive(false);
            }
        }

        public void PlayFadeIn(Color color, float animationTime, System.Action onFadedIn = null)
        {
            gameObject.SetActive(true);
            alphaColorAnimation.colorHolder.Color = color;
            alphaColorAnimation.Animate(animationTime, true, onFadedIn);
        }

        public void PlayFadeIn(float animationTime, System.Action onFadedIn)
        {
            PlayFadeIn(Color.white, animationTime, onFadedIn);
        }

        public void PlayFadeIn(Color color, System.Action onFadedIn = null)
        {
            PlayFadeIn(color, DefaultAnimationTime, onFadedIn);
        }

        public void PlayFadeIn(System.Action onFadedIn = null)
        {
            PlayFadeIn(Color.white, DefaultAnimationTime, onFadedIn);
        }

        public void PlayFadeOut(System.Action onFadedOut = null)
        {
            gameObject.SetActive(true);
            alphaColorAnimation.Animate(() =>
            {
                gameObject.SetActive(false);
                onFadedOut.InvokeSafely();
            }, false);
        }
    }
}
