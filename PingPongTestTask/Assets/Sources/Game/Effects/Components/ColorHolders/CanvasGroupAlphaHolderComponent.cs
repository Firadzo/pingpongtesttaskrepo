﻿using UnityEngine;
using PingPong.Core;

namespace PingPong.Game.Effects
{
	public class CanvasGroupAlphaHolderComponent : CacheComponentOfType<CanvasGroup>, IColorHolder
	{
		private void Awake( )
		{
			if( cachedComponent == null )
			{
				Init();
			}
		}

		public Color Color
		{
			get {
				return new Color( 0, 0, 0, cachedComponent.alpha );
			}
			set {
				cachedComponent.alpha = value.a;
			}
		}

		public float Alpha
		{
			get {
				return cachedComponent.alpha;
			}
			set {
				cachedComponent.alpha = value;
			}
		}
	}

    public class CanvasGroupAlphaHolder : IColorHolder
    {
        protected CanvasGroup canvasGroup;

        public Color Color
        {
            get
            {
                return new Color(0, 0, 0, canvasGroup.alpha);
            }
            set
            {
                canvasGroup.alpha = value.a;
            }
        }

        public float Alpha
        {
            get
            {
                return canvasGroup.alpha;
            }
            set
            {
                canvasGroup.alpha = value;
            }
        }

        public void Init()
        {
        }

        public CanvasGroupAlphaHolder(CanvasGroup canvasGroup) { this.canvasGroup = canvasGroup; }
    }
}
