﻿
using UnityEngine;
using PingPong.Core;

namespace PingPong.Game.Effects
{
    public class SpriteRendererColorHolderComponent : CacheComponentOfType<SpriteRenderer>, IColorHolder
    {
        private void Awake()
        {
            if (cachedComponent == null)
            {
                Init();
            }
        }

        public Color Color
        {
            get
            {
                return cachedComponent.color;
            }
            set
            {
                cachedComponent.color = value;
            }
        }

        public float Alpha
        {
            get
            {
                return cachedComponent.color.a;
            }
            set
            {
                Color color = cachedComponent.color;
                color.a = value;
                cachedComponent.color = color;
            }
        }
    }

    public class SpriteRendererColorHolder : IColorHolder
    {
        protected SpriteRenderer spriteRenderer;

        public Color Color
        {
            get
            {
                return spriteRenderer.color;
            }
            set
            {
                spriteRenderer.color = value;
            }
        }

        public float Alpha
        {
            get
            {
                return spriteRenderer.color.a;
            }
            set
            {
                Color color = spriteRenderer.color;
                color.a = value;
                spriteRenderer.color = color;
            }
        }

        public void Init()
        {
        }

        public SpriteRendererColorHolder(SpriteRenderer spriteRenderer) { this.spriteRenderer = spriteRenderer; }
    }

    public static class SpriteRendererColorHolderExtensions
    {
        public static SpriteRendererColorHolder GetColorHolder(this SpriteRenderer spriteRenderer)
        {
            return new SpriteRendererColorHolder(spriteRenderer);
        }

        public static SpriteRendererColorHolderComponent GetColorHolderComponent(this SpriteRenderer spriteRenderer)
        {
            return spriteRenderer.gameObject.AddComponent<SpriteRendererColorHolderComponent>();
        }
    }
}
