﻿using UnityEngine;
using UnityEngine.UI;
using PingPong.Core;

namespace PingPong.Game.Effects
{
    public class GenericUIGraphicColorHolderComponent<T> : CacheComponentOfType<T>, IColorHolder where T : Graphic
    {
        private void Awake()
        {
            if (cachedComponent == null)
            {
                Init();
            }
        }

        public Color Color
        {
            get
            {
                return cachedComponent.color;
            }
            set
            {
                cachedComponent.color = value;
            }
        }

        public float Alpha
        {
            get
            {
                return cachedComponent.color.a;
            }
            set
            {
                Color color = cachedComponent.color;
                color.a = value;
                cachedComponent.color = color;
            }
        }
    }

    public class UIGraphicColorHolder<T> : IColorHolder where T : Graphic
    {
        protected T graphic;

        public Color Color
        {
            get
            {
                return graphic.color;
            }
            set
            {
                graphic.color = value;
            }
        }

        public float Alpha
        {
            get
            {
                return graphic.color.a;
            }
            set
            {
                Color color = graphic.color;
                color.a = value;
                graphic.color = color;
            }
        }

        public void Init()
        {
        }

        public UIGraphicColorHolder(T graphic) { this.graphic = graphic; }
    }
}
