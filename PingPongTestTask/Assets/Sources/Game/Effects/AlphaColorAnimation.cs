﻿
using UnityEngine;
using PingPong.Core;

namespace PingPong.Game.Effects
{
    public class AlphaColorAnimation : MonoBehaviour
    {
        public enum EAnimationType
        {
            ONCE, LOOP, PING_PONG, PING_PONG_ONCE
        }

        #region VARIABLES
        public IColorHolder colorHolder;
        public bool setColorHolderAutomatically;

        public float fromAlpha;
        public float toAlpha;
        public float animationTime;

        public AnimationCurve effectCurve;
        public bool useCurve;
        public EAnimationType animationType = EAnimationType.ONCE;
        public bool autoStart;
        public bool disableObjectAtEnd;
        public bool useUnscaledTime;

        private float timerProgress;
        private bool forwardDirection = true;
        private BaseTimer timer;
        private System.Action onAnimationEnd;

        private bool inited = false;
        #endregion

        public bool IsPlaying => timer.isActive;

        protected void Awake()
        {
            Init();
            if (timer == null || !timer.isActive)
            {
                if (autoStart)
                {
                    Animate();
                }
                else
                {
                    enabled = false;
                }
            }
        }

        public void Init()
        {
            if (!inited)
            {
                inited = true;
                timer = new BaseTimer(animationTime, false, OnAnimationEnd, useUnscaledTime);
                if (colorHolder == null)
                {
                    var originalState = gameObject.activeSelf;
                    gameObject.SetActive(true);
                    if (setColorHolderAutomatically)
                    {
                        colorHolder = gameObject.GetColorHolder();
                    }
                    else
                    {
                        colorHolder = GetComponentInChildren<IColorHolder>();
                    }
                    gameObject.SetActive(originalState);
                }
                if (colorHolder != null)
                {
                    colorHolder.Init();
                }
            }
        }

        public void Pause()
        {
            timer.Stop();
        }

        public void Resume()
        {
            timer.Resume();
        }

        public void SetColor(Color color)
        {
            colorHolder.Color = color;
        }

        public void SetAlpha(float alpha)
        {
            colorHolder.Alpha = alpha;
        }

        public void RegisterCallbackOnAnimationEnd(System.Action callback)
        {
            onAnimationEnd = callback;
        }

        public void AddOnAnimationEndCallback(System.Action callback)
        {
            onAnimationEnd += callback;
        }

        public void Animate()
        {
            forwardDirection = true;
            Animate(animationTime);
        }

        public void Animate(System.Action callback, bool forward = true)
        {
            forwardDirection = forward;
            Animate(animationTime, fromAlpha, callback);
        }

        public void Animate(float time, float startAlpha, System.Action callback = null)
        {
            fromAlpha = startAlpha;
            colorHolder.Alpha = forwardDirection ? fromAlpha : toAlpha;
            timer.Start(time, callback);
            enabled = true;
            gameObject.SetActive(true);
        }

        public void Animate(float time, bool forward = true, System.Action callback = null)
        {
            forwardDirection = forward;
            Animate(time, fromAlpha, callback);
        }

        public void AnimateFromCurrentColor(float endAlpha, System.Action callback = null)
        {
            fromAlpha = colorHolder.Alpha;
            toAlpha = endAlpha;
            timer.Start(animationTime, callback);
            enabled = true;
            gameObject.SetActive(true);
        }

        protected void OnAnimationEnd()
        {
            if (onAnimationEnd != null)
            {
                onAnimationEnd();
            }
            if (disableObjectAtEnd)
            {
                gameObject.SetActive(false);
            }
        }

        protected void Update()
        {
            if (timer.isActive)
            {
                timer.Update();
                timerProgress = useCurve ? effectCurve.Evaluate(timer.NormalizedProgress) : timer.NormalizedProgress;
                colorHolder.Alpha = forwardDirection ? Mathf.Lerp(fromAlpha, toAlpha, timerProgress) : Mathf.Lerp(toAlpha, fromAlpha, timerProgress);
                if (!timer.isActive)
                {
                    OnAnimationEnd();
                    if (animationType == EAnimationType.LOOP)
                    {
                        timer.Start();
                    }
                    else if (animationType == EAnimationType.PING_PONG)
                    {
                        forwardDirection = !forwardDirection;
                        timer.Start();
                    }
                    else if (animationType == EAnimationType.PING_PONG_ONCE)
                    {
                        if (forwardDirection)
                        {
                            forwardDirection = !forwardDirection;
                            timer.Start();
                        }
                        else
                        {
                            enabled = false;
                        }
                    }
                    else
                    {
                        enabled = false;
                    }
                }
            }
        }
    }
}
