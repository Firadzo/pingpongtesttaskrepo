﻿
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace PingPong.Game.Effects
{
    public interface IColorHolder
    {
        Color Color
        {
            get; set;
        }

        float Alpha
        {
            get; set;
        }

        void Init();
    }

    public static class ColorHolderExtensions
    {
        public static IColorHolder GetColorHolder(this GameObject gameOjbect)
        {
            var spriteRenderer = gameOjbect.GetComponent<SpriteRenderer>();
            if (spriteRenderer != null)
            {
                return new SpriteRendererColorHolder(spriteRenderer);
            }

            var UIGraphic = gameOjbect.GetComponent<Graphic>();
            if (UIGraphic != null)
            {
                return new UIGraphicColorHolder<Graphic>(UIGraphic);
            }

            var canvasGroup = gameOjbect.GetComponent<CanvasGroup>();
            if (canvasGroup != null)
            {
                return new CanvasGroupAlphaHolder(canvasGroup);
            }

            Debug.LogWarning("Can't find suitable component to add color holder for object " + gameOjbect.name);
            return null;
        }

        public static IColorHolder GetColorHolderComponent(this GameObject gameOjbect, bool addColorHolderIfNull = true)
        {
            if (addColorHolderIfNull)
            {
                if (gameOjbect.GetComponent<SpriteRenderer>() != null)
                {
                    return gameOjbect.AddComponent<SpriteRendererColorHolderComponent>();
                }
                else if (gameOjbect.GetComponent<Image>() != null)
                {
                    return gameOjbect.AddComponent<UIImageColorHolderComponent>();
                }
                else if (gameOjbect.GetComponent<CanvasGroup>() != null)
                {
                    return gameOjbect.AddComponent<CanvasGroupAlphaHolderComponent>();
                }
                else
                {
                    Debug.LogWarning("Can't find suitable component to add color holder for object " + gameOjbect.name);
                }
            }
            return null;
        }
        
        public static void AnimateAlphaFade(this MonoBehaviour monoBehaviour, IColorHolder colorHolder, float time, bool fadeIn)
        {
            var startAlpha = fadeIn ? 0f : colorHolder.Alpha;
            var endAlpha = fadeIn ? 1f : 0f;
            monoBehaviour.StartCoroutine(AlphaFadeAnimation(colorHolder, time, startAlpha, endAlpha));
        }

        private static IEnumerator AlphaFadeAnimation(IColorHolder colorHolder, float time, float fromAlpha, float toAlpha)
        {
            var timer = 0f;
            while (timer < time)
            {
                timer += Time.deltaTime;
                colorHolder.Alpha = Mathf.Lerp(fromAlpha, toAlpha, timer / time);
                yield return null;
            }
        }
    }
}
