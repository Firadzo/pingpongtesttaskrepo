﻿
using System;
using UnityEngine;
using PingPong.Core;

namespace PingPong.Game.InputSytem
{
    public class InputController : IUpdatable
    {
        public event Action OnBackKeyPressed;

        public bool Enabled;

        public bool CanBeRemovedFromUpdate => false;

        public static InputController Instance { get; private set; }

        public static InputController Create()
        {
            if (Instance == null)
            {
                Instance = new InputController();
            }
            return Instance;
        }

        public InputController()
        {
            Enabled = true;
        }

        public void Reset()
        {
            OnBackKeyPressed = null;
        }

        public void UpdateObject(float deltaTime)
        {
            if (Enabled)
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    OnBackKeyPressed.InvokeSafely();
                }
            }
        }
    }
}
