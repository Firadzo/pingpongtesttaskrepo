﻿

namespace PingPong.Game
{
    public class MultiplayerMatchStarter
    {
        public static void StartMatch()
        {
            The.ScreenFadeEffect.PlayFadeIn(OnScreenFaded);
        }

        private static void OnScreenFaded()
        {
            The.UIManager.PrepareUIForMatch(UI.EMatchUIType.MultiplayerMatchUI);
            Match.CreateMultiPlayerMatch().PrepareGameField();
            The.ScreenFadeEffect.PlayFadeOut(OnScreenFadedOut);
        }

        private static void OnScreenFadedOut()
        {
            Match.Current.Start();
        }
    }
}
