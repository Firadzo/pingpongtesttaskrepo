﻿
using Photon.Pun;
using UnityEngine;

namespace PingPong.Game
{
    public class PlayerInputSync : MonoBehaviour
    {
        [SerializeField]
        private PhotonView photonView;

        public float    playerInput;
        public bool     isPause;

        public static PlayerInputSync This { get; private set; }
        public static PlayerInputSync Other { get; private set; }

        private void Awake()
        {
            photonView.GetComponentIfNull(ref photonView);

            if (photonView.IsMine)
            {
                This = this;
            }
            else
            {
                Other = this;
            }
        }

        public void SendData()
        {
            photonView.RPC("ReceivedData", RpcTarget.Others, new object[] { playerInput, isPause });
        }

        [PunRPC]
        private void ReceivedData(float playerInput, bool isPause, PhotonMessageInfo info)
        {
            this.playerInput = playerInput;
            this.isPause = isPause;
        }
    }
}
