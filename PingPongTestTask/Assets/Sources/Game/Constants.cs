﻿
using UnityEngine;

namespace PingPong.Game
{
    public static class Constants
    {
        public const string PlayerRacquetTag ="PlayerRacquet";

        public static readonly Color PlayerColor = new Color(0f,0.443f,0.741f,1f);
        public static readonly Color OpponentColor = new Color(0.698f,0.298f,0.341f,1f);
    }
}
