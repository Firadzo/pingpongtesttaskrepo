﻿
namespace PingPong.Game
{
    public abstract class Match
    {
        protected bool isStarted;

        protected BallController ball;
        protected RacquetController botRacquet;
        protected RacquetController topRacquet;

        protected int score;

        public bool IsPaused { get; protected set; }
        virtual public int Score => score;

        public static Match Current { get; private set; }

        public static Match CreateSinglePlayerMatch()
        {
            Current = new SinglePlayerMatch();
            return Current;
        }

        public static Match CreateMultiPlayerMatch()
        {
            Current = new MultiplayerMatch();
            return Current;
        }

        public static void DestroyIfExists()
        {
            if (Current != null)
            {
                Current.StopAndClearTheField();
                Current = null;
            }
        }

        virtual public void PrepareGameField()
        {
            The.GameField.SetupGameField();
            ball = The.GameField.Ball;
            botRacquet = The.GameField.BotRacquet;
            topRacquet = The.GameField.TopRacquet;

            SetupNewMatch();
        }

        abstract protected void SetupNewMatch();

        abstract public void Restart();

        abstract public void Start();

        abstract public void Stop();

        abstract public void StopAndClearTheField();

        virtual public void TogglePauseState(bool pauseEnabled)
        {
            IsPaused = pauseEnabled;
            ball.TogglePauseState(pauseEnabled);
        }
    }
}
