﻿
using UnityEngine;

namespace PingPong.Game.Data
{
    [CreateAssetMenu(fileName = "BallColorConfig", menuName = "Custom/BallColorConfig", order = 10)]
    public class BallColorConfig : ScriptableObject
    {
        private const string ResourcesPath = "GameData/BallColorConfig";

        [SerializeField]
        private Color[] Colors;

        public int ColorsNumber => Colors.Length;

        private static BallColorConfig loaded;
        public static BallColorConfig Get()
        {
            if (loaded == null)
            {
                Load();
            }
            return loaded;
        }

        private static void Load()
        {
            loaded = Resources.Load<BallColorConfig>(ResourcesPath);
        }

        public Color GetColorByIndex(int i)
        {
            return Colors[i];
        }
    }
}
