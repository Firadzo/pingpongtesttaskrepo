﻿
using UnityEngine;

namespace PingPong.Game.Data
{
    [CreateAssetMenu(fileName = "GameParametersConfig", menuName = "Custom/GameParametersConfig", order = 10)]
    public class GameParametersConfig : ScriptableObject
    {
        private const string ResourcesPath = "GameData/GameParametersConfig";

        public FloatRange ballMoveSpeedRange;
        public FloatRange ballSizeRange;
        public float MaxBallSpeedFactor;
        public float BallSpeedFactorChangeStep;
        [Space(10f)]
        public float racqueteMoveSpeed;

        private static GameParametersConfig loaded;
        public static GameParametersConfig Get()
        {
            if (loaded == null)
            {
                Load();
            }
            return loaded;
        }

        private static void Load()
        {
            loaded = Resources.Load<GameParametersConfig>(ResourcesPath);
        }
    }
}
