﻿
using UnityEngine;
using PingPong.Core;
using PingPong.Game.Data;

namespace PingPong.Game
{
    public class SinglePlayerMatch : Match, IUpdatable
    {
        protected float BallPushDelay = 1f;
        protected const float ObjectsZPosition = 10f;

        protected float currentBallSpeed;
        protected float currentBallSpeedFactor;

        protected BaseTimer ballActivationDelayTimer;
        protected GameParametersConfig gameParameters;

        public bool CanBeRemovedFromUpdate => false;

        public SinglePlayerMatch()
        {
            ballActivationDelayTimer = new BaseTimer();
        }

        override protected void SetupNewMatch()
        {
            currentBallSpeedFactor = 1f;
            gameParameters = GameParametersConfig.Get();
            var ballColor = BallColorConfig.Get().GetColorByIndex(The.PlayerProfile.BallColorIndex);
            currentBallSpeed = gameParameters.ballMoveSpeedRange.Random;

            ball.Setup(new Vector3(0f, 0f, ObjectsZPosition), gameParameters.ballSizeRange.Random, ballColor);
            botRacquet.Setup(new Vector3(0f, The.GameField.BotBorder, ObjectsZPosition), gameParameters.racqueteMoveSpeed);
            topRacquet.Setup(new Vector3(0f, The.GameField.TopBorder, ObjectsZPosition), gameParameters.racqueteMoveSpeed);
            botRacquet.tag = Constants.PlayerRacquetTag;
            topRacquet.tag = Constants.PlayerRacquetTag;
        }

        override public void Restart()
        {
            Stop();
            SetupNewMatch();
            Start();
        }

        override public void Start()
        {
            if (isStarted)
            {
                Debug.LogError("Match already started");
                return;
            }
            score = 0;
            IsPaused = false;
            isStarted = true;
            ball.HitedByPlayerRacquete += OnBallHitedByPlayerRacquete;
            ball.MovedOutOfGameField += OnBallMovedOutOfGameField;
            The.CommonUpdatableHolder.AddToUpdate(this);
            The.InputController.OnBackKeyPressed += OnBackKeyPressed;
            ballActivationDelayTimer.Start(BallPushDelay);
            ball.Push();
        }

        override public void Stop()
        {
            if (isStarted)
            {
                isStarted = false;
                ball.HitedByPlayerRacquete -= OnBallHitedByPlayerRacquete;
                ball.MovedOutOfGameField -= OnBallMovedOutOfGameField;
                The.InputController.OnBackKeyPressed -= OnBackKeyPressed;
                The.CommonUpdatableHolder.RemoveFromUpdate(this);
            }
        }

        override public void StopAndClearTheField()
        {
            Stop();
            The.PlayerProfile.SaveScore(score);
            The.GameField.Clear();
        }

        private void OnBackKeyPressed()
        {
            if (isStarted && !IsPaused)
            {
                TogglePauseState(true);
                The.UIManager.ShowPauseUI();
            }
        }

        private void OnBallHitedByPlayerRacquete()
        {
            score++;

            if (currentBallSpeedFactor < gameParameters.MaxBallSpeedFactor)
            {
                currentBallSpeedFactor += gameParameters.BallSpeedFactorChangeStep;
            }
            else
            {
                currentBallSpeedFactor = gameParameters.MaxBallSpeedFactor;
            }

        }

        private void OnBallMovedOutOfGameField(bool isTopBorder)
        {
            Stop();
            var isItNewRecord = The.PlayerProfile.SaveScore(score);
            The.UIManager.ShowEndGameMenu(isItNewRecord, score);
        }

        virtual public void UpdateObject(float deltaTime)
        {
            if (IsPaused)
            {
                return;
            }
            var moveDirection = 0f;

            if (Input.GetMouseButton(0))
            {
                var tapWorldPosition = Helpers.MainCamera.ScreenToWorldPoint(Input.mousePosition);
                moveDirection = tapWorldPosition.x > 0f ? 1f : -1f;
            }
#if UNITY_EDITOR
            else
            {
                var xAxis = Input.GetAxis("Horizontal");
                if (Mathf.Abs(xAxis) > 0.1f)
                {
                    moveDirection = xAxis > 0f ? 1f : -1f;
                }
            }
#endif

            topRacquet.UpdateObject(deltaTime, moveDirection);
            botRacquet.UpdateObject(deltaTime, moveDirection);
            if (ballActivationDelayTimer.isActive)
            {
                ballActivationDelayTimer.Update(deltaTime);
            }
            else
            {
                ball.UpdateObject(deltaTime, currentBallSpeed * currentBallSpeedFactor);
            }
        }

    }
}
