﻿
using UnityEngine;

namespace PingPong.Game
{
    public class BallNetworkPositionSync : BaseNetworkTransformSync
    {
        protected override Transform TargetTransform => GameField.Current.Ball == null ? null : GameField.Current.Ball.transform;

        public static BallNetworkPositionSync Current { get; private set; }

        private void Awake()
        {
            Current = this;
        }

        override public void Sync()
        {
            base.Sync();
            transform.localScale = TargetTransform.localScale;
        }

        protected override void ReadDataFromHost()
        {
            var ballPosition = transform.position;
            var scale = transform.localScale;
            ballPosition.y = -ballPosition.y;//For connected player field is upside down
            ballPosition.x = -ballPosition.x;
            TargetTransform.localScale = scale;
            TargetTransform.position = ballPosition;
        }
    }
}
