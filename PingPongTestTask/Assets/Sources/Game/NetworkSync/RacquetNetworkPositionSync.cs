﻿using Photon.Pun;
using UnityEngine;

namespace PingPong.Game
{
    public class RacquetNetworkPositionSync : MonoBehaviour
    {
        [SerializeField]
        private PhotonView photonView;

        public float X
        {
            get { return transform.position.x; }
            set
            {
                var position = transform.position;
                position.x = value;
                transform.position = position;
            }
        }

        public static RacquetNetworkPositionSync This { get; private set; }
        public static RacquetNetworkPositionSync Other { get; private set; }

        private void Awake()
        {
            photonView.GetComponentIfNull(ref photonView);

            if (photonView.IsMine)
            {
                This = this;
            }
            else
            {
                Other = this;
            }
        }
    }
}
