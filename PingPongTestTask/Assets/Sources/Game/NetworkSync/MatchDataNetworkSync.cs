﻿
using Photon.Pun;
using UnityEngine;

namespace PingPong.Game
{
    public class MatchDataNetworkSync : MonoBehaviour
    {
        [SerializeField]
        private PhotonView photonView;

        public int player1Score;
        public int player2Score;

        public static MatchDataNetworkSync Current { get; private set; }

        private void Awake()
        {
            Current = this;
            photonView.GetComponentIfNull(ref photonView);
        }

        public void SendData()
        {
            photonView.RPC("ReceivedData", RpcTarget.Others, new object[]
            { player1Score, player2Score });
        }

        [PunRPC]
        private void ReceivedData(int player1Score, int player2Score, PhotonMessageInfo info)
        {
            this.player1Score = player1Score;
            this.player2Score = player2Score;
        }
    }
}
