﻿using Photon.Pun;
using UnityEngine;

namespace PingPong.Game
{
    public abstract class BaseNetworkTransformSync : MonoBehaviour
    {
        protected abstract Transform TargetTransform { get; }

        virtual public void Sync()
        {
            transform.position = TargetTransform.position;
        }

        private void LateUpdate()
        {
            if (!PhotonNetwork.IsMasterClient && TargetTransform != null)
            {
                ReadDataFromHost();
            }
        }

        virtual protected void ReadDataFromHost()
        {
            TargetTransform.position = transform.position;
        }
    }
}
