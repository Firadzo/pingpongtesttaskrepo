﻿
using Photon.Pun;
using UnityEngine;

namespace PingPong.Game
{
    public class HostDataNetworkSync : MonoBehaviour
    {
        [SerializeField]
        private PhotonView photonView;

        public float fieldAspect;

        public static HostDataNetworkSync Current { get; private set; }

        private void Awake()
        {
            Current = this;
        }

        public void SendData()
        {
            photonView.GetComponentIfNull(ref photonView);
            photonView.RPC("ReceivedData", RpcTarget.Others, new object[] { fieldAspect });
        }

        [PunRPC]
        private void ReceivedData(float fieldAspect, PhotonMessageInfo info)
        {
            this.fieldAspect = fieldAspect;
        }
    }
}
