﻿
using UnityEngine;

namespace PingPong.Game
{
    public class BallController : MonoBehaviour
    {
        public event System.Action HitedByPlayerRacquete;
        public event System.Action<bool> MovedOutOfGameField;

        private float defaultSize;

        private Vector3 currentPosition;
        private Vector3 initialPosition;
        private Vector3 moveDirection;
        private float ballCheckRadius;
        private bool pushed;

        private Rigidbody2D m_RigidBody;
        private Rigidbody2D RigidBody
        {
            get
            {
                if (m_RigidBody == null)
                {
                    m_RigidBody = GetComponentInChildren<Rigidbody2D>();
                }
                return m_RigidBody;
            }
        }

        private SpriteRenderer m_SpriteRenderer;
        private SpriteRenderer SpriteRenderer
        {
            get
            {
                if (m_SpriteRenderer == null)
                {
                    m_SpriteRenderer = GetComponentInChildren<SpriteRenderer>();
                }
                return m_SpriteRenderer;
            }
        }

        public void Setup(Vector3 initialPosition, float defaultSize, Color color)
        {
            pushed = false;
            gameObject.SetActive(true);
            currentPosition = initialPosition;
            this.initialPosition = initialPosition;
            transform.position = initialPosition;
            RigidBody.position = initialPosition;
            RigidBody.velocity = Vector2.zero;
            this.defaultSize = defaultSize;
            transform.localScale = new Vector3(defaultSize, defaultSize, defaultSize);
            SpriteRenderer.color = color;
            ballCheckRadius = SpriteRenderer.bounds.size.x * 0.4f;//Taking slightly smaller radius for check
        }

        public void TogglePauseState(bool isPause)
        {
            if (pushed)
            {
                if (isPause)
                {
                    moveDirection = RigidBody.velocity.normalized;
                    RigidBody.velocity = Vector2.zero;
                }
                else
                {
                    RigidBody.velocity = moveDirection;
                }
            }
        }

        public void Push()
        {
            var randomRad = Random.value * (2 * Mathf.PI);
            moveDirection.x = Mathf.Cos(randomRad);
            moveDirection.y = Mathf.Sin(randomRad);
            RigidBody.velocity = moveDirection;
            pushed = true;
        }

        public void UpdateObject(float deltaTime, float moveSpeed)
        {
            RigidBody.velocity = RigidBody.velocity.normalized * moveSpeed;
        }

        private void FixedUpdate()
        {
            if (pushed)
            {
                currentPosition = RigidBody.position;
                if (currentPosition.y + ballCheckRadius > The.GameField.TopBorder || currentPosition.y - ballCheckRadius < The.GameField.BotBorder)
                {
                    pushed = false;
                    MovedOutOfGameField.InvokeSafely(currentPosition.y > 0f);
                    return;
                }
            }
            else
            {
                RigidBody.velocity = Vector2.zero;
            }
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            var collisionContact = collision.GetContact(0);
            moveDirection = Vector2.Reflect(moveDirection, collisionContact.normal);
            DebugTools.DrawCross(collisionContact.point, 10f, Color.red, 3f);
            Debug.DrawLine(collisionContact.point, collisionContact.point + collisionContact.normal * 100f, Color.red, 10f);
            Debug.DrawLine(collisionContact.point, collisionContact.point + (Vector2)moveDirection * 100f, Color.blue, 10f);

            if (collisionContact.collider.CompareTag(Constants.PlayerRacquetTag))
            {
                HitedByPlayerRacquete.InvokeSafely();
            }
        }

#if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.magenta;
            var position = transform.position;
            var targetPoint = position + moveDirection * 100f;
            Gizmos.DrawLine(position, targetPoint);
            Gizmos.DrawSphere(targetPoint, 5f);

            Gizmos.DrawWireSphere(transform.position, ballCheckRadius);
        }
#endif

    }
}
