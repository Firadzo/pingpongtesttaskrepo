﻿
using UnityEngine;

namespace PingPong.Game
{
    public class SinglePlayerMatchStarter
    {
        public static void StartMatch()
        {
            The.ScreenFadeEffect.PlayFadeIn(OnScreenFaded);
        }

        private static void OnScreenFaded()
        {
            The.UIManager.PrepareUIForMatch(UI.EMatchUIType.None);
            Match.CreateSinglePlayerMatch().PrepareGameField();
            The.ScreenFadeEffect.PlayFadeOut(OnScreenFadedOut);
        }

        private static void OnScreenFadedOut()
        {
            Match.Current.Start();
        }
    }
}
