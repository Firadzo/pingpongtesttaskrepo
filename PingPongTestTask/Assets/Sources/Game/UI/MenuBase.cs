﻿
using UnityEngine;

namespace PingPong.Game.UI
{
    public abstract class MenuBase : MonoBehaviour
    {
        protected event System.Action MenuHidden;

        [SerializeField]
        private CanvasGroup windowCanvasGroup;

        virtual public bool CanBeClosedByBackKey => true;
        virtual public bool CacheMenu => true;

        virtual public bool IsShowing { get; protected set; }
        virtual public bool IsHiding { get; protected set; }

        private bool m_Interactable;
        virtual public bool Interactable
        {
            get
            {
                return m_Interactable;
            }
            set
            {
                m_Interactable = value;
                if (windowCanvasGroup != null)
                {
                    windowCanvasGroup.interactable = m_Interactable;
                }
            }
        }

        virtual public bool IsActive => gameObject.activeSelf;
        virtual public bool IsAnimationPlaying => false;

        virtual public void Show()
        {
            gameObject.SetActive(true);
            Interactable = true;
        }

        virtual public void Hide()
        {
            OnMenuHidden();
        }

        virtual protected void OnMenuHidden()
        {
            IsShowing = false;
            IsHiding = false;
            if (CacheMenu)
            {
                gameObject.SetActive(false);
            }
            else
            {
                Destroy(gameObject);
            }
            MenuHidden.InvokeSafely();
        }

        virtual public void OnBackKeyPressed() { }
    }
}
