﻿
using UnityEngine;
using UnityEngine.UI;

namespace PingPong.Game.UI
{
    public class PauseMenu : MenuWithAnimatorBase
    {
        private string CurrentScoreLabelFormat ="Score: {0}";

        [SerializeField]
        private Text currentScoreLabel;
        [Space(10f)]
        [SerializeField]
        private Button restartGameButton;
        [SerializeField]
        private Button returnToGameButton;
        [SerializeField]
        private Button exitGameButton;

        private void Awake()
        {
            restartGameButton.onClick.AddListener(OnRestartGameClicked);
            returnToGameButton.onClick.AddListener(OnReturnToGameClicked);
            exitGameButton.onClick.AddListener(OnExitGameButtonClicked);
        }

        public void SetMultiplayerView()
        {
            restartGameButton.gameObject.SetActive(false);
        }

        public void SetOrdinalView()
        {
            restartGameButton.gameObject.SetActive(true);
        }

        public void OnEnable()
        {
            currentScoreLabel.text = string.Format(CurrentScoreLabelFormat, Match.Current.Score);
        }

        override public void OnBackKeyPressed()
        {
            OnReturnToGameClicked();
        }

        private void OnRestartGameClicked()
        {
            Hide(RestartGame);
        }

        private void OnReturnToGameClicked()
        {
            Hide(ReturnToGame);
        }

        private void OnExitGameButtonClicked()
        {
            Hide(ExitToMainMenu);
        }

        private void RestartGame()
        {
            MenuHidden -= RestartGame;
            SinglePlayerMatch.Current.Restart();
        }

        private void ReturnToGame()
        {
            MenuHidden -= ReturnToGame;
            SinglePlayerMatch.Current.TogglePauseState(false);
        }

        private void ExitToMainMenu()
        {
            MenuHidden -= ExitToMainMenu;
            The.UIManager.SwitchToMainMenu();
        }
    }
}
