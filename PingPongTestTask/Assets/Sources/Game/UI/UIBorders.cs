﻿
using UnityEngine;

namespace PingPong.Game.UI
{
    public class UIBorders : MonoBehaviour
    {
        [SerializeField]
        private RectTransform leftBorder;
        [SerializeField]
        private RectTransform rightBorder;

        public void Show(float borderWidth)
        {
            gameObject.SetActive(true);
            var sizeDelta = leftBorder.rect.size;
            sizeDelta.x = borderWidth;
            leftBorder.sizeDelta = sizeDelta;

            sizeDelta = rightBorder.rect.size;
            sizeDelta.x = borderWidth;
            rightBorder.sizeDelta = sizeDelta;
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}
