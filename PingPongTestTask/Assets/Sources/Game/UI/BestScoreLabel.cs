﻿
using UnityEngine;
using UnityEngine.UI;

namespace PingPong.Game.UI
{
    public class BestScoreLabel : MonoBehaviour
    {
        private const string LabelTextFormat = "Best Score: {0}";
        [SerializeField]
        private Text label;

        private void OnEnable()
        {
            label.text = string.Format(LabelTextFormat, The.PlayerProfile.BestScore);
        }
    }
}
