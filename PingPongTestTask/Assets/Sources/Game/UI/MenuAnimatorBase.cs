﻿
using UnityEngine;

namespace PingPong.Game
{
    public abstract class MenuAnimatorBase : MonoBehaviour
    {
        virtual public bool IsPlaying { get; protected set; }

        abstract public void PlayFadeIn(System.Action callback);
        abstract public void PlayFadeOut(System.Action callback);
    }
}
