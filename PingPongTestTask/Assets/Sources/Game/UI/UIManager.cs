﻿
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PingPong.Game.UI
{
    public class UIManager : MonoSingleton<UIManager>
    {
        private const string ResourcesFolderPathFormat ="UI/{0}";

        [SerializeField]
        private UIBorders UIBorders;
        [SerializeField]
        private RectTransform matchUILayer;

        private GameObject currentMatchUI;
        private Dictionary<EMenuType,MenuBase> cachedMenus;

        public MenuBase CurrentMenu { get; private set; }

        override protected void Awake()
        {
            base.Awake();
            UIBorders.Hide();
            cachedMenus = new Dictionary<EMenuType, MenuBase>();
            SwitchCurrentMenuTo(EMenuType.MainMenu);

            The.InputController.OnBackKeyPressed += OnBackKeyPressed;
        }

        public void ShowBorders(float borderWidth)
        {
            UIBorders.Show(borderWidth);
        }

        public void PrepareUIForMatch(EMatchUIType matchUIType)
        {
            SetupMatchUI(matchUIType);
            HideCurrentMenu();
        }

        public void ShowPauseUI(bool multiplayerView = false)
        {
            var menu = GetMenu<PauseMenu>(EMenuType.PauseMenu);
            if (multiplayerView)
            {
                menu.SetMultiplayerView();
            }
            else
            {
                menu.SetOrdinalView();
            }
            SetuCurrentMenu(menu);
        }

        public void ShowPlayerDisconnectedUI()
        {
            SwitchCurrentMenuTo(EMenuType.PlayerDisconnectedMenu);
        }

        public void ShowSettingsMenu()
        {
            SwitchToMenuWithFade(EMenuType.SettingsMenu);
        }

        public void SwitchToMainMenu()
        {
            SwitchToMenuWithFade(EMenuType.MainMenu, OnSwitchingToMainMenu);
        }

        public void ShowMultiplayerEndGameMenu(bool isWin)
        {
            var endGameMenu = GetMenu<EndGameMenu>(EMenuType.EndGameMenu);
            endGameMenu.SetMultiplayerEndGameView(isWin);
            SetuCurrentMenu(endGameMenu);
        }

        public void ShowEndGameMenu(bool newRecord, int score)
        {
            var endGameMenu = GetMenu<EndGameMenu>(EMenuType.EndGameMenu);
            endGameMenu.SetData(newRecord, score);
            SetuCurrentMenu(endGameMenu);
        }

        private void SwitchCurrentMenuTo(EMenuType menuType)
        {
            SetuCurrentMenu(GetMenu<MenuBase>(menuType));
        }

        private void OnSwitchingToMainMenu()
        {
            Match.DestroyIfExists();
            UIBorders.Hide();
            SetupMatchUI(EMatchUIType.None);
        }

        private void SwitchToMenuWithFade(EMenuType menuType, System.Action actionOnFullFade = null)
        {
            if (CurrentMenu != null)
            {
                CurrentMenu.Interactable = false;
            }
            The.ScreenFadeEffect.PlayFadeIn(() =>
            {
                actionOnFullFade.InvokeSafely();
                SetuCurrentMenu(GetMenu<MenuBase>(menuType));
                The.ScreenFadeEffect.PlayFadeOut();
            });
        }

        private void SetuCurrentMenu(MenuBase menu)
        {
            HideCurrentMenu();
            CurrentMenu = menu;
            CurrentMenu.Show();
        }

        private void SetupMatchUI(EMatchUIType matchUIType)
        {
            if (currentMatchUI != null)
            {
                Destroy(currentMatchUI);
            }
            if (matchUIType != EMatchUIType.None)
            {
                var resourcePath = string.Format(ResourcesFolderPathFormat, matchUIType.ToString());
                currentMatchUI = LoadUI(resourcePath, matchUILayer);
                currentMatchUI.transform.SetAsFirstSibling();
            }
        }

        private T GetMenu<T>(EMenuType menuType) where T : MenuBase
        {
            cachedMenus.TryGetValue(menuType, out var menu);
            if (menu == null)
            {
                var resourcePath = string.Format(ResourcesFolderPathFormat, menuType.ToString());
                menu = LoadUI(resourcePath, transform).GetComponent<MenuBase>();
                menu.transform.SetAsLastSibling();
                cachedMenus[menuType] = menu;
            }
            return menu as T;
        }

        private GameObject LoadUI(string resourcePath, Transform parent)
        {
            var UIObject = Instantiate(Resources.Load(resourcePath), parent) as GameObject;
            var rectTransform = UIObject.transform as RectTransform;
            rectTransform.anchorMin = new Vector2(0f, 0f);
            rectTransform.anchorMax = new Vector2(1f, 1f);
            return UIObject;
        }

        private void HideCurrentMenu()
        {
            if (CurrentMenu != null && CurrentMenu.IsActive)
            {
                CurrentMenu.Hide();
                CurrentMenu = null;
            }
        }

        private void OnBackKeyPressed()
        {
            if (The.ScreenFadeEffect.IsPlaying)
            {
                return;
            }
            if (CurrentMenu != null && CurrentMenu.IsActive && !CurrentMenu.IsAnimationPlaying && CurrentMenu.Interactable)
            {
                CurrentMenu.OnBackKeyPressed();
            }
        }
    }
}
