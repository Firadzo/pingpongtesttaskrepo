﻿
using UnityEngine;
using UnityEngine.UI;

namespace PingPong.Game.UI
{
    public class PlayerDisconnectedMenu : MenuWithAnimatorBase
    {
        [Space(10f)]
        [SerializeField]
        private Button exitGameButton;

        private void Awake()
        {
            exitGameButton.onClick.AddListener(OnExitGameButtonClicked);
        }

        override public void OnBackKeyPressed()
        {
            OnExitGameButtonClicked();
        }

        private void OnExitGameButtonClicked()
        {
            Hide(ExitToMainMenu);
        }

        private void ExitToMainMenu()
        {
            MenuHidden -= ExitToMainMenu;
            The.UIManager.SwitchToMainMenu();
        }
    }
}
