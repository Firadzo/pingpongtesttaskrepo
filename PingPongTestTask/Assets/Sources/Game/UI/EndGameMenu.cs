﻿
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace PingPong.Game.UI
{
    public class EndGameMenu : MenuWithAnimatorBase
    {
        private string CurrentScoreLabelFormat ="Current Score: {0}";
        private string NewRecordLabelFormat ="New Record: {0}!";
        private string YouAreWinnerLabelText ="You are Winner!";
        private string YouLostLabelText ="You lose :(";

        [Space(10f)]
        [SerializeField]
        private Text currentScoreLabel;
        [SerializeField]
        private Button restartGameButton;
        [SerializeField]
        private Button exitGameButton;

        private void Awake()
        {
            restartGameButton.onClick.AddListener(OnRestartGameClicked);
            exitGameButton.onClick.AddListener(OnExitGameButtonClicked);
        }

        public void SetMultiplayerEndGameView(bool isWin)
        {
            restartGameButton.gameObject.SetActive(false);
            currentScoreLabel.text = isWin ? YouAreWinnerLabelText : YouLostLabelText;
        }

        public void SetData(bool newRecord, int score)
        {
            restartGameButton.gameObject.SetActive(true);
            currentScoreLabel.text = string.Format(newRecord ? NewRecordLabelFormat : CurrentScoreLabelFormat, score);
        }

        override public void OnBackKeyPressed()
        {
            OnExitGameButtonClicked();
        }

        private void OnRestartGameClicked()
        {
            Hide(RestartGame);
        }

        private void OnExitGameButtonClicked()
        {
            Hide(ExitToMainMenu);
        }

        private void RestartGame()
        {
            MenuHidden -= RestartGame;
            SinglePlayerMatch.Current.Restart();
        }

        private void ExitToMainMenu()
        {
            MenuHidden -= ExitToMainMenu;
            The.UIManager.SwitchToMainMenu();
        }
    }
}
