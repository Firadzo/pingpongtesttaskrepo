﻿
using System.Collections;
using UnityEngine;

namespace PingPong.Game.UI
{
    public abstract class MenuWithAnimatorBase : MenuBase
    {
        protected event System.Action MenuShowed;

        [SerializeField]
        protected MenuAnimatorBase menuAnimator;

        override public bool IsAnimationPlaying => menuAnimator.IsPlaying;

        override public void Show()
        {
            if (IsShowing)
            {
                return;
            }
            base.Show();
            IsShowing = true;
            IsHiding = false;
            Interactable = false;
            menuAnimator.PlayFadeIn(OnMenuShowed);
        }

        override public void Hide()
        {
            if (IsHiding || !IsActive)
            {
                return;
            }
            IsShowing = false;
            IsHiding = true;
            Interactable = false;
            menuAnimator.PlayFadeOut(OnMenuHidden);
        }

        protected void Hide(System.Action actionAfterHide)
        {
            MenuHidden += actionAfterHide;
            Hide();
        }

        virtual protected void OnMenuShowed()
        {
            IsShowing = false;
            IsHiding = false;
            Interactable = true;
            MenuShowed.InvokeSafely();
        }
    }
}
