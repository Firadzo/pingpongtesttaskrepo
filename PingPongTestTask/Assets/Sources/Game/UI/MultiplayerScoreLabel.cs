﻿
using UnityEngine;
using UnityEngine.UI;

namespace PingPong.Game.UI
{
    public class MultiplayerScoreLabel : MonoBehaviour
    {
        public Text scoreLabel;
        public bool isMyScore;

        private int viewScoreValue;

        private MultiplayerMatch CurrentMatch => Match.Current as MultiplayerMatch;

        private int Score
        {
            get
            {
                if (CurrentMatch != null)
                {
                    return isMyScore ? CurrentMatch.Score : CurrentMatch.OpponentScore;
                }
                return 0;
            }
        }

        private void RefreshView(int score)
        {
            if (score != viewScoreValue)
            {
                viewScoreValue = score;
                scoreLabel.text = viewScoreValue.ToString();
            }
        }

        private void Awake()
        {
            viewScoreValue = 0;
            scoreLabel.text = viewScoreValue.ToString();
            scoreLabel.color = (isMyScore ? Constants.PlayerColor : Constants.OpponentColor).SetAlpha(0.5f);
        }

        private void Update()
        {
            RefreshView(Score);
        }
    }
}
