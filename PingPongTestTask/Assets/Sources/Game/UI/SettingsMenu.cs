﻿
using UnityEngine;
using UnityEngine.UI;
using PingPong.Game.Data;

namespace PingPong.Game.UI
{
    public class SettingsMenu : MenuBase
    {
        [SerializeField]
        private Image ballPreviewImage;
        [SerializeField]
        private GameObject selectedBallLabel;
        [Space(10f)]
        [SerializeField]
        private Button showPrevButton;
        [SerializeField]
        private Button showNextButton;
        [SerializeField]
        private Button selectBallButton;
        [SerializeField]
        private Button backButton;

        private int previewColorIndex;

        private BallColorConfig colorConfig;

        public override bool CacheMenu => false;

        private void Awake()
        {
            showPrevButton.onClick.AddListener(OnShowPreviousColorClicked);
            showNextButton.onClick.AddListener(OnShowNextColorClicked);
            selectBallButton.onClick.AddListener(OnSelectBallButtonClicked);
            backButton.onClick.AddListener(OnBackButtonClicked);
        }

        private void OnEnable()
        {
            colorConfig = BallColorConfig.Get();
            previewColorIndex = The.PlayerProfile.BallColorIndex;
            RefreshView();
        }

        override public void OnBackKeyPressed()
        {
            OnBackButtonClicked();
        }

        private void OnShowPreviousColorClicked()
        {
            previewColorIndex--;
            if (previewColorIndex < 0)
            {
                previewColorIndex = colorConfig.ColorsNumber - 1;
            }
            RefreshView();
        }

        private void OnShowNextColorClicked()
        {
            previewColorIndex++;
            if (previewColorIndex >= colorConfig.ColorsNumber)
            {
                previewColorIndex = 0;
            }
            RefreshView();
        }

        private void OnSelectBallButtonClicked()
        {
            The.PlayerProfile.BallColorIndex = previewColorIndex;
            RefreshView();
        }

        private void OnBackButtonClicked()
        {
            The.UIManager.SwitchToMainMenu();
        }

        private void RefreshView()
        {
            ballPreviewImage.color = colorConfig.GetColorByIndex(previewColorIndex);
            var isSelected = previewColorIndex == The.PlayerProfile.BallColorIndex;
            selectBallButton.gameObject.SetActive(!isSelected);
            selectedBallLabel.SetActive(isSelected);
        }
    }
}