﻿
using UnityEngine;
using PingPong.Core;
using PingPong.Game.Effects;

namespace PingPong.Game
{
    public class MenuAnimator : MenuAnimatorBase, IUpdatable
    {
        [SerializeField]
        private float animationTime;
        [SerializeField]
        private AlphaColorAnimation alphaFadeAnimation;
        [SerializeField]
        private RectTransform screenSizeRef;
        [SerializeField]
        private RectTransform transformToMove;
        [SerializeField]
        private Vector2 onScreenPosition;

        [SerializeField]
        private AnimationCurve moveCurve;

        private Vector2 outOffScreenPosition;

        private bool animatingFadeIn;

        private float timer;
        private System.Action callBack;

        public bool CanBeRemovedFromUpdate => !IsPlaying;

        private void CalculateOutOfScreenPosition()
        {
            var screenHeight = screenSizeRef.rect.size.y;
            var transformToMoveHeight = transformToMove.rect.size.y;

            outOffScreenPosition = onScreenPosition;
            outOffScreenPosition.y += (screenHeight + transformToMoveHeight) * 0.5f;
        }

        override public void PlayFadeIn(System.Action callback)
        {
            CalculateOutOfScreenPosition();
            this.callBack = callback;
            timer = 0f;
            IsPlaying = true;
            transformToMove.localPosition = outOffScreenPosition;
            alphaFadeAnimation.SetAlpha(0f);
            animatingFadeIn = true;
            alphaFadeAnimation.Animate(animationTime, animatingFadeIn);
            The.CommonUpdatableHolder.AddToUpdate(this);
        }

        override public void PlayFadeOut(System.Action callback)
        {
            CalculateOutOfScreenPosition();
            this.callBack = callback;
            timer = 0f;
            IsPlaying = true;
            animatingFadeIn = false;
            alphaFadeAnimation.Animate(animationTime, animatingFadeIn);
            The.CommonUpdatableHolder.AddToUpdate(this);
        }

        public void UpdateObject(float deltaTime)
        {
            timer += deltaTime;

            var animProgress = moveCurve.Evaluate(timer / animationTime);
            transformToMove.localPosition = animatingFadeIn ? Vector2.Lerp(outOffScreenPosition, onScreenPosition, animProgress) :
                                                                Vector2.Lerp(onScreenPosition, outOffScreenPosition, animProgress);

            if (timer >= animationTime)
            {
                IsPlaying = false;
                callBack.InvokeSafely();
                The.CommonUpdatableHolder.RemoveFromUpdate(this);
            }
        }

#if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            if (screenSizeRef != null && transformToMove != null)
            {
                CalculateOutOfScreenPosition();

                Gizmos.color = Color.yellow;
                Gizmos.DrawLine(transformToMove.TransformPoint(onScreenPosition), transformToMove.TransformPoint((outOffScreenPosition)));
                Gizmos.DrawWireCube(transformToMove.TransformPoint(outOffScreenPosition), transformToMove.rect.size.MultiplyVector2(transformToMove.lossyScale));
            }
        }
#endif
    }
}
