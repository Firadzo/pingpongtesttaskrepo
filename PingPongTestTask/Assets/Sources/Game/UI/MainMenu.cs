﻿
using UnityEngine;
using UnityEngine.UI;

namespace PingPong.Game.UI
{
    public class MainMenu : MenuBase
    {
        [SerializeField]
        private Button startSingleGameButton;
        [SerializeField]
        private Button findMatchButton;
        [SerializeField]
        private Button settingsButton;
        [SerializeField]
        private Button exitGameButton;

        [Space(10f)]
        [SerializeField]
        private GameObject matchSearchOverlay;
        [SerializeField]
        private Button cancelMatchSearchButton;

        private void Awake()
        {
            startSingleGameButton.onClick.AddListener(OnStartSingleGameButtonClicked);
            findMatchButton.onClick.AddListener(OnFindMatchButtonClicked);
            settingsButton.onClick.AddListener(OnSettingsButtonClicked);
            exitGameButton.onClick.AddListener(OnExitGameButtonClicked);
            cancelMatchSearchButton.onClick.AddListener(OnCancelMatchSearchButtonClicked);
        }

        private void OnEnable()
        {
            matchSearchOverlay.gameObject.SetActive(false);
        }

        private void OnStartSingleGameButtonClicked()
        {
            SinglePlayerMatchStarter.StartMatch();
        }

        private void OnFindMatchButtonClicked()
        {
            if (NetworkMatchSearcher.Current == null)
            {
                NetworkMatchSearcher.Run();
                matchSearchOverlay.gameObject.SetActive(true);
                NetworkMatchSearcher.Current.OnMatchWasFound += OnMatchWasFound;
            }
        }

        private void OnMatchWasFound()
        {
            matchSearchOverlay.GetComponentInChildren<Text>().text = "Waiting for opponent...";
            MultiplayerMatchStarter.StartMatch();
        }

        private void OnSettingsButtonClicked()
        {
            The.UIManager.ShowSettingsMenu();
        }

        private void OnExitGameButtonClicked()
        {
            Application.Quit();
        }

        private void OnCancelMatchSearchButtonClicked()
        {
            if (NetworkMatchSearcher.Current != null)
            {
                NetworkMatchSearcher.Current.Stop();
            }
            matchSearchOverlay.gameObject.SetActive(false);
        }
    }
}
