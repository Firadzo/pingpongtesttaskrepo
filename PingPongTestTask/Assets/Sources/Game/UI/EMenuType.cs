﻿
namespace PingPong.Game.UI
{
    public enum EMenuType
    {
        MainMenu,
        SettingsMenu,
        PauseMenu,
        EndGameMenu,
        PlayerDisconnectedMenu
    }
}
