﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Helpers
{
    private static Camera cachedMainCamera;
    public static Camera MainCamera
    {
        get
        {
            if (cachedMainCamera == null)
            {
                cachedMainCamera = Camera.main;
            }
            return cachedMainCamera;
        }
    }

    public static Vector3 UIWorldPosToCameraWorldPos(Vector3 uiPos, RectTransform uiObjectCanvas, Camera camera = null)
    {
        Vector2 viewPortPos = uiPos - uiObjectCanvas.position;

        Vector2 sizeDelta = uiObjectCanvas.sizeDelta;
        sizeDelta.x *= uiObjectCanvas.localScale.x;
        sizeDelta.y *= uiObjectCanvas.localScale.y;

        viewPortPos += sizeDelta / 2f;
        viewPortPos.x /= sizeDelta.x;
        viewPortPos.y /= sizeDelta.y;

        if (camera == null)
        {
            return MainCamera.ViewportToWorldPoint(viewPortPos);
        }
        else
        {
            return camera.ViewportToWorldPoint(viewPortPos);
        }
    }

    public static RaycastHit2D Raycast2D(Vector2 point)
    {
        return Physics2D.Raycast(point, Vector2.zero, 0f);
    }

    public static RaycastHit2D Raycast2D(Camera camera)
    {
        return Raycast2D(camera.ScreenToWorldPoint(Input.mousePosition));
    }

    public static RaycastHit2D Raycast2D(int layer)
    {
        return Physics2D.Raycast(MainCamera.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, 0f, layer);
    }

    /// <summary>
    /// By default we use main camera
    /// </summary>
    public static RaycastHit2D Raycast2D()
    {
        return Raycast2D(MainCamera);
    }

    public static Color SetAlpha(this Color color, float alpha)
    {
        color.a = alpha;
        return color;
    }

    public static Color MultipleRGB(this Color color, float factor)
    {
        color.r *= factor;
        color.g *= factor;
        color.b *= factor;
        return color;
    }

    public static void Set2DPosition(Transform transf, Vector2 pos)
    {
        Vector3 localPos = transf.localPosition;
        localPos.x = pos.x;
        localPos.y = pos.y;
        transf.localPosition = localPos;
    }

    public static void SetZLocalPos(Transform transf, float z)
    {
        Vector3 pos = transf.localPosition;
        pos.z = z;
        transf.localPosition = pos;
    }

    public static void SetZWorldPos(Transform transf, float z)
    {
        Vector3 pos = transf.position;
        pos.z = z;
        transf.position = pos;
    }

    public static void SetZWorldPos(float z, params Transform[] transforms)
    {
        Vector3 pos;
        foreach (Transform transf in transforms)
        {
            pos = transf.position;
            pos.z = z;
            transf.position = pos;
        }
    }

    public static void FlipTransformX(Transform transform)
    {
        Vector3 localScale = transform.localScale;
        localScale.x *= -1f;
        transform.localScale = localScale;
    }

    public static void FlipTransformX(Transform transform, bool positive)
    {
        Vector3 localScale = transform.localScale;
        if (localScale.x < 0 && positive || localScale.x > 0 && !positive)
        {
            localScale.x *= -1f;
        }
        transform.localScale = localScale;
    }

    public static void SetLocalXYToZero(Transform transf)
    {
        transf.localPosition = new Vector3(0, 0, transf.localPosition.z);
    }

    public static float GetAngleBetween(Vector2 vector1, Vector2 vector2)
    {
        float dot = vector1.x * vector2.x + vector1.y * vector2.y;
        float det = vector1.x * vector2.y - vector1.y * vector2.x;
        return Mathf.Rad2Deg * Mathf.Atan2(det, dot);
    }

    public static Vector2 RotateVector2d(Vector2 vector, float degrees)
    {
        degrees *= Mathf.Deg2Rad;
        float sin = Mathf.Sin(degrees);
        float cos = Mathf.Cos(degrees);
        Vector2 result = Vector2.zero;
        result.x = vector.x * cos - vector.y * sin;
        result.y = vector.x * sin + vector.y * cos;
        return result;
    }

    public static bool IsPointInsideOfRect(Vector2 rectCenter, Vector2 rectHalfSize, Vector2 point)
    {
        return (rectCenter.y + rectHalfSize.y >= point.y && rectCenter.y - rectHalfSize.y <= point.y &&
                rectCenter.x - rectHalfSize.x <= point.x && rectCenter.x + rectHalfSize.x >= point.x);
    }

    public static Vector2 ClampPointToRect(Vector2 rectCenter, Vector2 rectHalfSize, Vector2 point)
    {
        if (point.x > rectCenter.x + rectHalfSize.x)
        {
            point.x = rectCenter.x + rectHalfSize.x;
        }
        else if (point.x < rectCenter.x - rectHalfSize.x)
        {
            point.x = rectCenter.x - rectHalfSize.x;
        }

        if (point.y > rectCenter.y + rectHalfSize.y)
        {
            point.y = rectCenter.y + rectHalfSize.y;
        }
        else if (point.y < rectCenter.y - rectHalfSize.y)
        {
            point.y = rectCenter.y - rectHalfSize.y;
        }
        return point;
    }

    //http://stackoverflow.com/questions/15594424/line-crosses-rectangle-how-to-find-the-cross-points
    public static List<Vector2> GetIntersectionPoint(Vector2 lineStart, Vector2 lineEnd, Rect rect)
    {
        List<Vector2> intersections = new List<Vector2>();
        Vector2? intersetionPoint;
        // Top line
        intersetionPoint = GetIntersectionPointBetweenTwoLines(lineStart, lineEnd, new Vector2(rect.xMin, rect.yMax), new Vector2(rect.xMax, rect.yMax));
        if (intersetionPoint != null)
        {
            intersections.Add((Vector2)intersetionPoint);
        }
        // Bottom line
        intersetionPoint = GetIntersectionPointBetweenTwoLines(lineStart, lineEnd, new Vector2(rect.xMin, rect.yMin), new Vector2(rect.xMax, rect.yMin));
        if (intersetionPoint != null)
        {
            intersections.Add((Vector2)intersetionPoint);
        }
        // Left side...
        intersetionPoint = GetIntersectionPointBetweenTwoLines(lineStart, lineEnd, new Vector2(rect.xMin, rect.yMin), new Vector2(rect.xMin, rect.yMax));
        if (intersetionPoint != null)
        {
            intersections.Add((Vector2)intersetionPoint);
        }
        // Right side
        intersetionPoint = GetIntersectionPointBetweenTwoLines(lineStart, lineEnd, new Vector2(rect.xMax, rect.yMin), new Vector2(rect.xMax, rect.yMax));
        if (intersetionPoint != null)
        {
            intersections.Add((Vector2)intersetionPoint);
        }
        return intersections;

    }

    //http://stackoverflow.com/questions/15594424/line-crosses-rectangle-how-to-find-the-cross-points
    public static Vector2? GetIntersectionPointBetweenTwoLines(Vector2 startPoint1, Vector2 endPoint1, Vector2 startPoint2, Vector2 endPoint2)
    {

        float x1 = startPoint1.x;
        float y1 = startPoint1.y;
        float x2 = endPoint1.x;
        float y2 = endPoint1.y;

        float x3 = startPoint2.x;
        float y3 = startPoint2.y;
        float x4 = endPoint2.x;
        float y4 = endPoint2.y;
        //Debug.DrawLine(startPoint1, endPoint1, Color.red, 2f);
        //Debug.DrawLine(startPoint2, endPoint2, Color.blue, 2f);
        float d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
        if (d != 0)
        {
            float xi = ((x3 - x4) * (x1 * y2 - y1 * x2) - (x1 - x2) * (x3 * y4 - y3 * x4)) / d;
            if (!IsPointOnLine(xi, x1, x2))
            { return null; }
            if (!IsPointOnLine(xi, x3, x4))
            { return null; }

            float yi = ((y3 - y4) * (x1 * y2 - y1 * x2) - (y1 - y2) * (x3 * y4 - y3 * x4)) / d;
            if (!IsPointOnLine(yi, y1, y2))
            { return null; }
            if (!IsPointOnLine(yi, y3, y4))
            { return null; }

            //DebugUtilities.DrawDebugCrossAtPoint(new Vector2(xi, yi), Color.yellow, 2f, 0.5f );
            return new Vector2(xi, yi);
        }
        return null;
    }

    public static bool IsPointOnLine(float point, float start, float end)
    {
        if (end == start)
        { return true; }
        bool positioveDirection = end > start;
        if (positioveDirection && (point < start || point > end))
        { return false; }
        if (!positioveDirection && (point > start || point < end))
        { return false; }
        return true;
    }
}
