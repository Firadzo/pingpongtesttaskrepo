﻿
using UnityEngine;

public static partial class Extensions
{
    #region DEBUG
    public static void DebugObjectHierarchy(this GameObject obj)
    {
        DebugObjectHierarchy(obj.transform);
    }

    public static void DebugObjectHierarchy(this Transform transform)
    {
        string nextSymbol = " -> ";
        System.Text.StringBuilder strBuilder = new System.Text.StringBuilder();
        strBuilder.AppendFormat("DebugObjectHierarchy: {0}", transform.name);
        strBuilder.AppendLine("");
        while (transform.parent != null)
        {
            strBuilder.Append(transform.name);
            transform = transform.parent;
            if (transform.parent != null)
            {
                strBuilder.Append(nextSymbol);
            }
        }
        Debug.Log(strBuilder.ToString());
    }
    #endregion
}
