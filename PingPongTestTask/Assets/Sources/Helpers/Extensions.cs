﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static partial class Extensions
{
    public static void GetComponentIfNull<T>(this GameObject gameObj, ref T result) where T : Component
    {
        if (result == null)
        {
            result = gameObj.GetComponent<T>();
        }
    }

    public static void GetComponentIfNull<T>(this Behaviour behaviour, ref T result) where T : Component
    {
        if (result == null)
        {
            result = behaviour.GetComponent<T>();
        }
    }

    public static void GetComponentInChildrenIfNull<T>(this Behaviour behaviour, ref T result) where T : Component
    {
        if (result == null)
        {
            result = behaviour.GetComponentInChildren<T>();
        }
    }

    public static Canvas GetFirstRootCanvasInParent(this Transform transform)
    {
        Canvas[] canvasInParent = transform.GetComponentsInParent<Canvas>();
        for (int i = 0; i < canvasInParent.Length; i++)
        {
            if (canvasInParent[i].isRootCanvas)
            {
                return canvasInParent[i];
            }
        }
        return null;
    }

    public static void SetLayerForAllInChildAndParent(this GameObject gameObject, int layer)
    {
        gameObject.layer = layer;
        var transform = gameObject.transform;
        for (int i = 0; i < transform.childCount; i++)
        {
            var child = transform.GetChild(i);
            if (child != null)
            {
                child.gameObject.SetLayerForAllInChildAndParent(layer);
            }
        }
    }

    public static void SetLayerForAllInChildAndParen(this GameObject gameObject, int layer, out Dictionary<int, int> oldLayersData)
    {
        oldLayersData = new Dictionary<int, int>();
        oldLayersData.Add(gameObject.GetInstanceID(), gameObject.layer);
        gameObject.layer = layer;
        var transform = gameObject.transform;
        var childCount = transform.childCount;
        for (int i = 0; i < childCount; i++)
        {
            var child = transform.GetChild(i);
            if (child != null)
            {
                child.gameObject.SetLayerForAllInChildAndParent(layer);
            }
        }
    }

    public static void SetLayersForAllInChildAndParent(this GameObject gameObject, Dictionary<int, int> layersData)
    {
        int layer;
        if (layersData.TryGetValue(gameObject.GetInstanceID(), out layer))
        {
            gameObject.layer = layer;
        }
        var transform = gameObject.transform;
        var childCount = transform.childCount;
        for (int i = 0; i < childCount; i++)
        {
            var child = transform.GetChild(i);
            if (child != null)
            {
                child.gameObject.SetLayersForAllInChildAndParent(layersData);
            }
        }
    }

    public static void ToggleComponent<T>(this GameObject gameObject, bool enabled) where T : Behaviour
    {
        T component = gameObject.GetComponent<T>();
        if (component != null)
        {
            component.enabled = enabled;
        }
    }

    public static bool IsNullOrEmpty<T>(this T[] array)
    {
        return array == null || array.Length == 0;
    }

    public static bool IsNullOrEmpty<T>(this List<T> list)
    {
        return list == null || list.Count == 0;
    }

    public static void InvokeSafely(this System.Action action)
    {
        if (action != null)
        {
            action();
        }
    }

    public static void InvokeSafely<T1>(this System.Action<T1> action, T1 t1)
    {
        if (action != null)
        {
            action(t1);
        }
    }

    public static void InvokeSafely<T1, T2>(this System.Action<T1, T2> action, T1 t1, T2 t2)
    {
        if (action != null)
        {
            action(t1, t2);
        }
    }

    public static byte[] GetBytes(this string str)
    {
        byte[] bytes = new byte[str.Length * sizeof(char)];
        System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
        return bytes;
    }

    public static string GetString(this byte[] bytes)
    {
        char[] chars = new char[bytes.Length / sizeof(char)];
        System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
        return new string(chars);
    }

    public static Vector2 MultiplyVector2(this Vector2 vector2, Vector2 multiplyBy)
    {
        vector2.x *= multiplyBy.x;
        vector2.y *= multiplyBy.y;
        return vector2;
    }

    public static Vector3 MultiplyVector3(this Vector3 vector3, Vector3 multiplyBy)
    {
        vector3.x *= multiplyBy.x;
        vector3.y *= multiplyBy.y;
        vector3.z *= multiplyBy.z;
        return vector3;
    }

    public static void DisableParticlesEmission(this GameObject obj)
    {
        ParticleSystem[] particleSystems = obj.GetComponentsInChildren<ParticleSystem>();
        for (int i = 0; i < particleSystems.Length; i++)
        {
            ParticleSystem.EmissionModule emissionModule = particleSystems[i].emission;
            emissionModule.enabled = false;
        }
    }

    public static Coroutine CallDelayedWithCoroutine(this MonoBehaviour monoBehaviour, float delay, System.Action callback, bool unscaledTime = false)
    {
        return monoBehaviour.StartCoroutine(CallbackCoroutine(delay, callback, unscaledTime));
    }
    
    public static IEnumerator WaitCoroutine(this MonoBehaviour monoBehaviour, float waitTime)
    {
        var timer = 0f;
        while (timer < waitTime)
        {
            timer += Time.deltaTime;
            yield return null;
        }
    }

    private static IEnumerator CallbackCoroutine(float delay, System.Action callback, bool unscaledTime = false)
    {
        if (unscaledTime)
        {
            yield return new WaitForSecondsRealtime(delay);
        }
        else
        {
            yield return new WaitForSeconds(delay);
        }
        callback.InvokeSafely();
    }
}
